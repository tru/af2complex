# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

FROM  ghcr.io/truatpasteurdotfr/alphafold-jupyter-pymol:main

# Install conda packages.
ENV PATH="/opt/conda/bin:$PATH"

RUN	cd /app && \
	git clone https://gitlab.pasteur.fr/tru/af2complex.git && \
	cd af2complex && \
	pip3 install networkx==2.5.0 --no-cache-dir 
#	pip3 install -r requirements.txt --no-cache-dir 
#  yields:
# ...
#Installing collected packages: numpy, networkx, tensorflow-cpu
#  Attempting uninstall: numpy
#    Found existing installation: numpy 1.23.5
#    Uninstalling numpy-1.23.5:
#      Successfully uninstalled numpy-1.23.5
#Successfully installed networkx-2.5 numpy-1.21.6 tensorflow-cpu-2.9.0
